const Swal = require('sweetalert2');

window.confirmDelete = (formId) => {
    Swal.fire({
        title: 'Are you sure?',
        text: 'Delete event?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(response => {
        if (response.isConfirmed) {
            $(document).ready(() => {
                $("#" + formId).submit();
            })

        }
    })
}

window.confirmApprove = () => {
    Swal.fire({
        title: 'Are you sure?',
        text: 'Approve event? (This action cannot be undone).',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(response => {
        if (response.isConfirmed) {
            $(document).ready(() => {
                $("#approve_event").submit();
            })

        }
    })
}

window.confirmBan = () => {
    Swal.fire({
        title: 'Are you sure?',
        text: 'Ban event? (This action cannot be undone).',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
    }).then(response => {
        if (response.isConfirmed) {
            $(document).ready(() => {
                $("#ban_event").submit();
            })

        }
    })
}

function readURL(input) {
    if (input.files && input.files[0]) {
        $("#image_preview").css("display", "flex");
        $("#image_preview_none").css("display", "none");

        var reader = new FileReader();

        reader.onload = function (e) {
            $('#event_banner').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
    else {
        $("#image_preview").css("display", "none");
        $("#image_preview_none").css("display", "flex");
    }
}



$(document).ready(function () {
    $("#start_date").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });

    $("#end_date").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });

    $("#start_time").timepicker({
        timeFormat: 'h:mm p',
        interval: 1,
        minTime: '12.00am',
        maxTime: '11.59pm',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $("#end_time").timepicker({
        timeFormat: 'h:mm p',
        interval: 1,
        minTime: '12.00am',
        maxTime: '11.59pm',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $("#imgInp").change(function () {
        readURL(this);
    });
})

