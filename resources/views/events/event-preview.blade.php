@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-8">

      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Preview</li>
        </ol>
      </nav>

      @foreach ($eventss as $events)
      <div class="card">

        <div class="card-header">
          Event Preview
        </div>
        <div class="card-body">


          <div class="container">
            @if(session()->has('success'))
            <div class="row">
              <div class="alert alert-success col-sm">
                {{ session()->get('success') }}
              </div>
            </div>
            @endif
            @if(session()->has('error'))
            <div class="row">
              <div class="alert alert-danger col-sm">
                {{ session()->get('error') }}
              </div>
            </div>
            @endif
            <div class="row">
              <div style="display: flex; align-items: center; justify-content: center; width: 1920px; height: 480px; overflow:auto">
                <img style="width: 100%; height: 100%; background-size: cover;" src="{{ asset('storage/' . $events->banner_path) }}" alt="No Image :(" />
              </div>
            </div>
            <div class="row justify-content-start">
              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Event Name </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{$events->name}}
                </p>
              </div>


              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Event Start Date </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{date('d-m-Y', strtotime($events->start_date))}}
                </p>
              </div>






              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Event Start Date </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{date('d-m-Y', strtotime($events->end_date))}}
                </p>
              </div>

              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Event Start Time </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{date('h:i A', strtotime($events->start_time))}}
                </p>
              </div>


              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Event End Time </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{date('h:i A', strtotime($events->end_time))}}
                </p>
              </div>

              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Description </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{$events->description}}
                </p>
              </div>

              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Organizer </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{$events->organizer}}
                </p>
              </div>

              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Event Location </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{$events->location_event}}
                </p>
              </div>

              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Person In Charge </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{$events->pic_username}}
                </p>
              </div>
              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Contact Number </strong>
                <p className="form-control-static text-complete text-uppercase">
                  {{$events->pic_contact_no}}
                </p>
              </div>

              <div class="col-sm-12 col-md-4 mt-3">
                <strong> Social Link </strong>
                <br>
                <a href="{{ $events->social_link }}" className="form-control-static text-complete text-uppercase">
                  {{$events->social_link}}
                </a>
              </div>
            </div>

          </div>

        </div>



      </div>
      @endforeach

    </div>
  </div>
</div>
@endsection