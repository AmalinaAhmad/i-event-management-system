@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if (!Route::is('event-list'))
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/event-list">Events</a></li>
                    @if(Route::is('create-event'))
                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                    @endif
                    @if(Route::is('update-event'))
                    <li class="breadcrumb-item active" aria-current="page">Update</li>
                    @endif
                </ol>
            </nav>
            @endif
            @if (Route::is('event-list') && Auth::user()->is_admin == 0)
            <div class="row">
                <h3 class="text-center col-sm">Your Events</h3>
            </div>
            @endif
            @if (Route::is('event-list') && Auth::user()->is_admin == 1 && is_null(request()->history))
            <div class="row">
                <h3 class="text-center col-sm">Events</h3>
            </div>
            @endif
            @if(Route::is('event-list') && Auth::user()->is_admin == 1 && request()->history == 'true')
            <div class="row">
                <h3 class="text-center col-sm">History</h3>
            </div>
            @endif
            @if(session()->has('success'))
            <div class="row">
                <div class="alert alert-success col-sm">
                    {{ session()->get('success') }}
                </div>
            </div>
            @endif
            @if(session()->has('error'))
            <div class="row">
                <div class="alert alert-danger col-sm">
                    {{ session()->get('error') }}
                </div>
            </div>
            @endif
            @if (Route::is('create-event'))
            <div class="card">
                <div class="card-header">
                    Create Event
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('create-event') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div id="image_preview" style="display: none; align-items: center; justify-content: center; width: 1920px; height: 480px">
                                <img id="event_banner" style="width: 100%; height: 100%; background-size: cover;" src="#" alt="No Image :(" />
                            </div>
                            <div id="image_preview_none" style="display: flex; align-items: center; justify-content: center; width: 1920px; height: 480px">
                                Event Banner Preview Here
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="event_banner" class="col-md-4 col-form-label text-md-right">Event Banner</label>

                            <div class="col-md-6">
                                <input id="imgInp" type="file" accept="image/png, image/jpeg, image/jpg" class=" @error('event_banner') is-invalid @enderror" name="event_banner" value="{{ old('event_banner') }}" required>
                                @error('event_banner')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Event Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_date" class="col-md-4 col-form-label text-md-right">Event Start Date</label>

                            <div class="col-md-6">
                                <input id="start_date" type="text" class="form-control @error('start_date') is-invalid @enderror" name="start_date" value="{{ old('start_date') }}" autocomplete="off" required autofocus>

                                @error('start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_date" class="col-md-4 col-form-label text-md-right">Event End Date</label>

                            <div class="col-md-6">
                                <input id="end_date" type="text" class="form-control @error('end_date') is-invalid @enderror" name="end_date" value="{{ old('end_date') }}" autocomplete="off" required autofocus>

                                @error('end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_time" class="col-md-4 col-form-label text-md-right">Event Start Time</label>

                            <div class="col-md-6">
                                <input id="start_time" type="text" class="form-control @error('start_time') is-invalid @enderror" name="start_time" value="{{ old('start_time') }}" autocomplete="off" required autofocus>

                                @error('start_time')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_time" class="col-md-4 col-form-label text-md-right">Event End Time</label>

                            <div class="col-md-6">
                                <input id="end_time" type="text" class="form-control @error('end_time') is-invalid @enderror" name="end_time" value="{{ old('end_time') }}" autocomplete="off" required autofocus>

                                @error('end_time')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pic_contact_no" class="col-md-4 col-form-label text-md-right">PIC Contact Number</label>

                            <div class="col-md-6">
                                <input id="pic_contact_no" type="text" class="form-control @error('pic_contact_no') is-invalid @enderror" name="pic_contact_no" value="{{ old('pic_contact_no') }}" required autofocus>

                                @error('pic_contact_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="social_link" class="col-md-4 col-form-label text-md-right">Social/Site URL Link</label>

                            <div class="col-md-6">
                                <input id="social_link" type="text" class="form-control @error('social_link') is-invalid @enderror" name="social_link" value="{{ old('social_link') }}" required autofocus>

                                @error('social_link')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Event Description</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autofocus>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="organizer" class="col-md-4 col-form-label text-md-right">Organizer</label>

                            <div class="col-md-6">
                                <input id="organizer" type="text" class="form-control @error('organizer') is-invalid @enderror" name="organizer" value="{{ old('organizer') }}" required autofocus>

                                @error('organizer')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="location_event" class="col-md-4 col-form-label text-md-right">Event Location</label>

                            <div class="col-md-6">
                                <input id="location_event" type="text" class="form-control @error('location_event') is-invalid @enderror" name="location_event" value="{{ old('location_event') }}" required autofocus>

                                @error('location_event')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @if (Route::is('update-event'))
            <div class="card">
                <div class="card-header">
                    Update Event
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('update-event') }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $event->id }}">
                        <div class="form-group row">
                            <div id="image_preview" style="display: none; align-items: center; justify-content: center; width: 1920px; height: 480px">
                                <img id="event_banner" style="width: 100%; height: 100%; background-size: cover;" src="#" alt="No Image :(" />
                            </div>
                            <div id="image_preview_none" style="display: flex; align-items: center; justify-content: center; width: 1920px; height: 480px">
                                <img style="width: 100%; height: 100%; background-size: cover;" src="{{ asset('storage/' . $event->banner_path) }}" alt="No Image :(" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="event_banner" class="col-md-4 col-form-label text-md-right">Event Banner</label>

                            <div class="col-md-6">
                                <input id="imgInp" type="file" accept="image/png, image/jpeg, image/jpg" class=" @error('event_banner') is-invalid @enderror" name="event_banner" value="{{ old('event_banner') }}">
                                @error('event_banner')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Event Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $event->name }}" required>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_date" class="col-md-4 col-form-label text-md-right">Event Start Date</label>

                            <div class="col-md-6">
                                <input id="start_date" type="text" class="form-control @error('start_date') is-invalid @enderror" name="start_date" value="{{ date('d-m-Y', strtotime($event->start_date)) }}" autocomplete="off" required>

                                @error('start_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_date" class="col-md-4 col-form-label text-md-right">Event End Date</label>

                            <div class="col-md-6">
                                <input id="end_date" type="text" class="form-control @error('end_date') is-invalid @enderror" name="end_date" value="{{ date('d-m-Y', strtotime($event->end_date)) }}" autocomplete="off" required>

                                @error('end_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="start_time" class="col-md-4 col-form-label text-md-right">Event Start Time</label>

                            <div class="col-md-6">
                                <input id="start_time" type="text" class="form-control @error('start_time') is-invalid @enderror" name="start_time" value="{{ date('h:i A', strtotime($event->start_time)) }}" autocomplete="off" required>

                                @error('start_time')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_time" class="col-md-4 col-form-label text-md-right">Event End Time</label>

                            <div class="col-md-6">
                                <input id="end_time" type="text" class="form-control @error('end_time') is-invalid @enderror" name="end_time" value="{{ date('h:i A', strtotime($event->end_time)) }}" autocomplete="off" required>

                                @error('end_time')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pic_contact_no" class="col-md-4 col-form-label text-md-right">PIC Contact Number</label>

                            <div class="col-md-6">
                                <input id="pic_contact_no" type="text" class="form-control @error('pic_contact_no') is-invalid @enderror" name="pic_contact_no" value="{{ $event->pic_contact_no }}" required>

                                @error('pic_contact_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="social_link" class="col-md-4 col-form-label text-md-right">Social/Site URL Link</label>

                            <div class="col-md-6">
                                <input id="social_link" type="text" class="form-control @error('social_link') is-invalid @enderror" name="social_link" value="{{ $event->social_link }}" required>

                                @error('social_link')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">Event Description</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $event->description }}" required>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="organizer" class="col-md-4 col-form-label text-md-right">Organizer</label>

                            <div class="col-md-6">
                                <input id="organizer" type="text" class="form-control @error('organizer') is-invalid @enderror" name="organizer" value="{{ $event->organizer }}" required>

                                @error('organizer')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="location_event" class="col-md-4 col-form-label text-md-right">Event Location</label>

                            <div class="col-md-6">
                                <input id="location_event" type="text" class="form-control @error('location_event') is-invalid @enderror" name="location_event" value="{{ $event->location_event }}" required>

                                @error('location_event')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @if (Route::is('event-list'))
            <!-- <div class="card-header">
                    Events
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Event Name</th>
                                    <th scope="col">Event Date</th>
                                    <th scope="col">Social Link</th>
                                    <th scope="col">Created at</th>
                                    <th scope="col" class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($events as $event)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td> {{ $event->name }} </td>
                                    <td> {{ date('d-m-Y', strtotime($event->start_date)) }} </td>
                                    <td> {{ $event->social_link }} </td>
                                    <td> {{ date('d-m-Y', strtotime($event->created_at)) }} </td>
                                    <td class="text-center">
                                        <div style="display: flex; justify-content: center">
                                            <button class="btn btn-primary btn-sm" onclick="" title="Update event"><i class="fas fa-pencil-alt"></i></button>
                                            <form id="delete{{ $event->id }}" action="{{ route('event-delete') }}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $event->id }}">
                                                <button class="btn btn-danger btn-sm" type="button" onclick="confirmDelete('delete{{ $event->id }}')" title="Delete event"><i class="fas fa-trash"></i></button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @if (count($events) == 0)
                                <tr>
                                    <td colspan=6>
                                        <div class="alert alert-success">
                                            You have no event(s) created yet.
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div> -->
            @if(count($events) === 0 && Auth::user()->is_admin == 0)
            <div class="row">
                <div class="alert alert-success col-sm">
                    You currently have no event(s).
                </div>
            </div>
            @endif
            @if(count($events) === 0 && Auth::user()->is_admin == 1)
            <div class="row">
                <div class="alert alert-success col-sm">
                    No event(s).
                </div>
            </div>
            @endif
            <div class="row">
                @foreach ($events as $event)
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-sm-12 p-0" style="position: relative; text-align: center">
                                    <img src="{{ asset('storage/' . $event->banner_path) }}" alt="Event Image Here" style="width:100%; height:auto; border-radius: 20px">
                                    <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: rgba(0,0,0,0.5); width:100%; height:100%; color:white; display:flex; justify-content:center; align-items:center; flex-direction:column; border-radius: 20px">
                                        <h3>{{ $event->name }}</h3>
                                        <h5>{{ $event->description }}</h5>
                                        @if($event->is_approved == true && $event->is_banned == false)
                                        <h5><strong>(Approved)</strong></h5>
                                        @endif
                                        @if($event->is_approved == true && $event->is_banned == true)
                                        <h5><strong>(Banned)</strong></h5>
                                        @endif
                                        <div style="display: flex;">
                                            <a href="/event-details?id={{ $event->id }}">
                                                <button class="btn btn-outline-light text-white" title="View event details"><i class="fas fa-eye"></i></button>
                                            </a>
                                            @if (Auth::user()->is_admin == 0 && $event->is_approved == false)
                                            <a href="/update-event?id={{ $event->id }}">
                                                <button class="btn btn-outline-light text-white ml-1 mr-1" title="Update event"><i class="fas fa-pencil-alt"></i></button>
                                            </a>
                                            <form id="delete{{ $event->id }}" action="{{ route('event-delete') }}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $event->id }}">
                                                <button class="btn btn-outline-light text-white" type="button" onclick="confirmDelete('delete{{ $event->id }}')" title="Delete event"><i class="fas fa-trash"></i></button>
                                            </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
</div>
<script src="{{ asset('js/event.js') }}" defer></script>
@endsection