@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div id="preview" class="carousel slide mb-3" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($events as $event)
                        @if($loop->index == 0)
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ asset('storage/' . $event->banner_path) }}" alt="Event Image">
                            <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: rgba(0,0,0,0.5); width:100%; height:50%; color:white; display:flex; justify-content:center; align-items:center; flex-direction:column">
                                <h3>{{ $event->name }}</h3>
                                <h5>{{ $event->description }}</h5>
                            </div>
                        </div>
                        @endif
                        @if($loop->index > 0)
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ asset('storage/' . $event->banner_path) }}" alt="Event Image">
                            <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: rgba(0,0,0,0.5); width:100%; height:50%; color:white; display:flex; justify-content:center; align-items:center; flex-direction:column">
                                <h3>{{ $event->name }}</h3>
                                <h5>{{ $event->description }}</h5>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#preview" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#preview" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                @foreach ($events as $event)
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-sm-12 p-0" style="position: relative; text-align: center">
                                    <img src="{{ asset('storage/' . $event->banner_path) }}" alt="Event Image Here" style="width:100%; height:auto">
                                    <div style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: rgba(0,0,0,0.5); width:100%; height:100%; color:white; display:flex; justify-content:center; align-items:center; flex-direction:column">
                                        <h3>{{ $event->name }}</h3>
                                        <h5>{{ $event->description }}</h5>
                                        <div style="display: flex;">
                                            <a href="/event-preview?id={{ $event->id }}">
                                                <button class="btn btn-outline-light text-white" title="View event details"><i class="fas fa-eye"></i></button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection