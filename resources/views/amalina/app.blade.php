<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <title>I-Event Management</title>
</head>

<body>
    @section('sidebar')
    This is the master sidebar.
    @show

    @yield('content')
</body>

</html>