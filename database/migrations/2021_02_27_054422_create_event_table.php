<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->unsignedBigInteger('pic_id');
            $table->foreign('pic_id')->references('id')->on('users');
            $table->string('pic_contact_no');
            $table->string('banner_path');
            $table->string('social_link');
            $table->string('description');
            $table->string('organizer');
            $table->string('location_event');
            $table->boolean('is_approved')->default(false);
            $table->unsignedBigInteger('approved_by')->nullable()->default(null);
            $table->foreign('approved_by')->references('id')->on('users');
            $table->boolean('is_banned')->default(false);
            $table->unsignedBigInteger('banned_by')->nullable()->default(null);
            $table->foreign('banned_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event');
    }
}
