<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'ahmad',
            'email' => 'ahmad0609firdaus@yopmail.com',
            'password' => Hash::make('1234'),
        ]);

        DB::table('users')->insert([
            'username' => 'amalina',
            'email' => 'amalinaahmad@yopmail.com',
            'password' => Hash::make('1234'),
        ]);

        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@yopmail.com',
            'password' => Hash::make('1234'),
            'is_admin' => 1
        ]);
    }
}
