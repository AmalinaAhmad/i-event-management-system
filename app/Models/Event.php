<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $table = 'event';

    protected $fillable = [
        'name', 'start_date', 'end_date' ,  'start_time' ,  'end_time' , 'pic_id', 'pic_contact_no', 'banner_path', 'social_link' , 'description',  'organizer', 'location_event', 'is_approved, approved_by, is_banned'
    ];
}
