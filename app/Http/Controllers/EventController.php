<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Throwable;

class EventController extends Controller
{
    public function create(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'pic_contact_no' => 'required',
            'event_banner' => 'image|mimes:jpeg,jpg,png|required',
            'social_link' => 'required',
            'description' => 'required',
            'organizer' => 'required',
            'location_event' => 'required'
        ]);

        // $path = Storage::putFile('uploads', $request->file('event_banner'), 'public');
        $path = Storage::disk('public')->put('uploads', $request->file('event_banner'));

        try {
            $new_event = new Event;
            $new_event->name = $request->name;
            $new_event->start_date = date("Y-m-d", strtotime($request->start_date));
            $new_event->end_date = date("Y-m-d", strtotime($request->end_date));
            $new_event->start_time = date("Y-m-d H:i:s", strtotime($request->start_time));
            $new_event->end_time = date("Y-m-d H:i:s", strtotime($request->end_time));
            $new_event->pic_id = Auth::user()->id;
            $new_event->pic_contact_no = $request->pic_contact_no;
            //store and get banner path
            $new_event->banner_path = $path;
            $new_event->social_link = $request->social_link;
            $new_event->description = $request->description;
            $new_event->organizer = $request->organizer;
            $new_event->location_event = $request->location_event;
            $new_event->save();
            return redirect()->route('event-list')
                ->with('success', 'Event created successfully.');
        } catch (Throwable $error) {
            // dd($error);
            return redirect()->route('create-event')
                ->with('error', 'Event error .');
        }
    }

    public function find(Request $request)
    {
        try {
            if (Auth::user()->is_admin == 0) {
                $events = Event::where("pic_id", Auth::user()->id)
                ->join("users", "users.id", "=", "pic_id")
                ->orderBy("updated_at", "DESC")
                ->get(['users.username AS pic_username', 'event.*']);
            }

            if (Auth::user()->is_admin == 1) {
                $events = Event::where("is_approved", false)
                ->orderBy("created_at", "ASC")
                ->get(['event.*']);
            }

            if (Auth::user()->is_admin ==1 && $request->history == "true") {
                $events = Event::where("is_approved", true)
                ->orderBy("created_at", "DESC")
                ->get(['event.*']);
            }
            

            return view("events.index")->with(compact('events'));
        } catch (Throwable $error) {
            // dd($error);
            return view("events.index")->with('error', 'Cannot find user event(s).');
        }
    }

    public function findToUpdate(Request $request)
    {
        $request->validate([
            'id' => 'required'
        ]);

        try {
            $event = Event::find($request->id);
            return view("events.index")->with(compact('event'));
        } catch (Throwable $error) {
            dd($error);
            return view("events.index")->with('error', 'No event found.');
        }
    }

    public  function findByID( Request $request){
        try {
            $eventss = Event::where("event.id", $request->id)
                ->join("users", "users.id", "=", "event.pic_id")
                ->orderBy("updated_at", "DESC")
                ->get(['users.username AS pic_username', 'event.*']);

                if(Auth::check()) {
                    return view("events.event-details")->with(compact('eventss'));
                }
                else{
                    return view("events.event-preview")->with(compact('eventss'));
                }

            
        } catch (Throwable $error) {
            // dd($error);
            if (Auth::check()) {
                return view("events.event-details")->with('error', 'Cannot find user event(s).');
            }
            else{
                return view("events.preview-event")->with('error', 'Cannot find event(s).');
            }
            
        }
    }

    public function delete(Request $request)
    {

        $request->validate([
            'id' => 'required',
        ]);

        try {
            $event = Event::find($request->id);

            if (!$event) {
                return redirect()->route('event-list')->with('error', 'Event not existed.');
            }

            $result = $event->delete();

            if (!$result) {
                return redirect()->route('event-list')->with('error', 'Cannot delete event.');
            }

            return redirect()->route('event-list')->with('success', 'Event deleted.');
        } catch (Throwable $error) {
            // dd($error);
            return redirect()->route('event-list')->with('error', 'Cannot delete event.');
        }
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'pic_contact_no' => 'required',
            'event_banner' => 'image|mimes:jpeg,jpg,png',
            'social_link' => 'required',
            'description' => 'required',
            'organizer' => 'required',
            'location_event' => 'required'
        ]);

        try {
            $event = Event::find($request->id);

            $event->name = $request->name;
            $event->start_date = date("Y-m-d", strtotime($request->start_date));
            $event->end_date = date("Y-m-d", strtotime($request->end_date));
            $event->start_time = date("Y-m-d H:i:s", strtotime($request->start_time));
            $event->end_time = date("Y-m-d H:i:s", strtotime($request->end_time));
            $event->pic_id = Auth::user()->id;
            $event->pic_contact_no = $request->pic_contact_no;

            if (!is_null($request->event_banner)) {
                $path = Storage::disk('public')->put('uploads', $request->file('event_banner'));
                $event->banner_path = $path;
            }

            $event->social_link = $request->social_link;
            $event->description = $request->description;
            $event->organizer = $request->organizer;
            $event->location_event = $request->location_event;
            $event->save();

            return redirect()->route('update-event', ['id' => $request->id])
                ->with('success', 'Event updated successfully.');
        } catch (Throwable $error) {
            // dd($error);
            return redirect()->route('update-event', ['id' => $request->id])
                ->with('error', 'Cannot update event.');
        }
    }

    public function approveEvent(Request $request) {

        $request->validate([
            'id' => 'required'
        ]);

        try{
            $event = Event::find($request->id);

            $event->is_approved = true;
            $event->approved_by = Auth::user()->id;
            $event->save();
            return redirect()->route('event-details', ['id' => $request->id])
                ->with('success', 'Event approved successfully.');

        }
        catch(Throwable $error) {
            // dd($error);
            return redirect()->route('event-details', ['id' => $request->id])
                ->with('error', 'Cannot approve event.');
        }
    }

    public function banEvent(Request $request) {

        $request->validate([
            'id' => 'required'
        ]);

        try{
            $event = Event::find($request->id);

            $event->is_banned = true;
            $event->banned_by = Auth::user()->id;
            $event->save();
            return redirect()->route('event-details', ['id' => $request->id])
                ->with('success', 'Event banned successfully.');

        }
        catch(Throwable $error) {
            // dd($error);
            return redirect()->route('event-details', ['id' => $request->id])
                ->with('error', 'Cannot ban event.');
        }
    }
}
