<?php

use App\Http\Controllers\EventController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/amalina', function () {
//     return view('amalina/app');
// });

// Route::get('/login', function () {
//     return view('amalina/login');
// });

// Route::get('/register', function () {
//     return view('amalina/register');
// });

// Route::resource('posts', PostController::class);

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get("/create-event", function () {
    return view("events/index");
})->name("create-event")->middleware(['auth', 'route.member']);

Route::get("/event-list", [EventController::class, "find"])->name("event-list")->middleware(['auth']);

Route::get("/event-details", [EventController::class, "findByID"])->name("event-details")->middleware(['auth']);

Route::post("/create-event", [EventController::class, "create"])->name("create-event");

Route::delete("/event-delete", [EventController::class, "delete"])->name("event-delete");

Route::get("/update-event", [EventController::class, "findToUpdate"])->name("update-event")->middleware(['auth', 'route.member']);

Route::put("/update-event", [EventController::class, "update"])->name("update-event");

Route::put("/approve-event", [EventController::class, "approveEvent"])->name("approve-event");

Route::put("/ban-event", [EventController::class, "banEvent"])->name("ban-event");

Route::get("/event-preview", [EventController::class, "findByID"])->name("event-preview");
